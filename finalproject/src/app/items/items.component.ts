import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../services/customer.service';
@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrl: './items.component.css'
})
export class ItemsComponent implements OnInit{
  items: any;
  showItemsDiv: boolean = true;
  constructor(private service: CustomerService) 
  { }

  ngOnInit(): void {
    // Initialization logic here if needed
  }
  getVegetable() {
    this.service.getByCategoryVegetable().subscribe((data: any) => {
      this.items = data;
      this.showItemsDiv = false;
    });
  }

  getDairy() {
    this.service.getByCategoryDairy().subscribe((data: any) => {
      this.items = data;
      this.showItemsDiv = false;
    });
  }

  getFruits() {
    this.service.getByCategoryFruits().subscribe((data: any) => {
      this.items = data;
      this.showItemsDiv = false;
    });
  }

  getGrocery() {
    this.service.getByCategoryGrocery().subscribe((data: any) => {
      this.items = data;
      this.showItemsDiv = false;
    });
  }
}
