import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../services/customer.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  emailId: any;
  password: any;
  captcha: string = '';
  captchaInput: string = '';
  captchaText: string = ''; 

  constructor(private router: Router, private service: CustomerService) { }

  ngOnInit(): void {
    this.emailId = '';
    this.password = '';
    this.generateCaptcha();
  }

  async loginSubmit() {
    try {
      const data = await this.service.custLogin({ emailId: this.emailId, password: this.password }).toPromise();
      console.log('Full Service Response:', data);

      if (data) {
        if (this.emailId === 'admin@gmail.com' && this.password === 'adminpassword') {
          // Navigate to the admin page
          this.router.navigate(['/admin']);
          return;
        }
        alert("User Login Successful");
      } else {
        alert("Login Failed. Invalid Credentials");
      }
    } catch (error) {
      console.error('Service Error:', error);
      alert("Login Failed. Please try again.");
    }
  }

  generateCaptcha(): void {
    const possibleChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const length = 6;
    this.captcha = Array.from({ length }, () =>
      possibleChars[Math.floor(Math.random() * possibleChars.length)]
    ).join('');
  }
}
