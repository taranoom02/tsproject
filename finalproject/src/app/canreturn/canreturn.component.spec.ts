import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CanreturnComponent } from './canreturn.component';

describe('CanreturnComponent', () => {
  let component: CanreturnComponent;
  let fixture: ComponentFixture<CanreturnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CanreturnComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CanreturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
