import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../services/customer.service';
import { NgForm } from '@angular/forms';
import zxcvbn from 'zxcvbn';




@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  checkPasswordStrength(password: string): { score: number, feedback: string } {
    const result = zxcvbn(password);
    return {
      score: result.score,
      feedback: result.feedback.suggestions.join(' ')
    };
  }
  
  custName: string = '';
  custPhone: string = '';
  custEmail: string = '';
  Password: string = '';
  confirmPassword: string = '';

  constructor(private router: Router, private service: CustomerService) { }

  ngOnInit(): void {
    // Any initialization logic you might have
  }

  registerSubmit(): void {
    const registerForm = {
      custName: this.custName,
      custPhone: this.custPhone,
      custEmail: this.custEmail,
      custPassword: this.Password,
      confirmPassword: this.confirmPassword
    };

    console.log('Form Data:', registerForm);
    console.log('Form Data before request:', registerForm);
    this.service.registerCustomer(registerForm).then((data: any) => {
      console.log(data);
      console.log('Response from server:', data);
      this.router.navigate(['/login']);
    }).catch(error => {
      console.error('Registration failed:', error);
    });
  }
}