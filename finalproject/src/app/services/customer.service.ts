import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  cartItems: any;
  

  constructor(private http: HttpClient) {
    this.cartItems = [];
  }
  addToCart(item: any) {
    this.cartItems.push(item);
  }
  

  registerCustomer(registerForm: any) {
    console.log('Request Body:', registerForm); 
    const body = {
      custEmail: registerForm.custEmail,
      custName: registerForm.custName,
      password: registerForm.custPassword,
      custPhone: registerForm.custPhone.toString(),
      confirmPassword: registerForm.confirmPassword
    };

    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.post('http://localhost:8080/regCustomer', body, { headers }).toPromise()
  }

 // customer.service.ts
custLogin(loginForm: any) {
  return this.http.get("http://localhost:8080/getCustByEmailAndPassword/" + loginForm.emailId + "/" + loginForm.password);
}
getAllCust(): any {
  return this.http.get('http://localhost:8080/Cust');
}
getByCategoryGrocery() {
  return this.http.get("http://localhost:8080/getProductByCategory/grocery");
}
getByCategoryVegetable() {
  return this.http.get("http://localhost:8080/getProductByCategory/vegetable");
}
getByCategoryFruits() {
  return this.http.get("http://localhost:8080/getProductByCategory/fruits");
}
getByCategoryDairy() {
  return this.http.get("http://localhost:8080/getProductByCategory/dairy");
}
getAllRecipes(){
  return this.http.get("http://localhost:8080/getAllRecipes");
}
getByRecipe(recipeName: string) {
  return this.http.get(`http://localhost:8080/getRecipesByName/${recipeName}`);
}
}
