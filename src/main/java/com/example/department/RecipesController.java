package com.example.department;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.dao.RecipesDAO;
import com.example.model.Recipes;

@RestController
@CrossOrigin(origins="http://localhost:4200") 
public class RecipesController {
@Autowired
RecipesDAO recipesDAO;
@GetMapping("/getAllRecipes")
public List<Recipes> getAllRecipes() {
    return recipesDAO.getAllRecipes();
}

@GetMapping("/getRecipesByName/{recipeName}")
public List<Recipes> getRecipesByName(@PathVariable String recipeName) {
    return recipesDAO.getRecipesByName(recipeName);
}
}
