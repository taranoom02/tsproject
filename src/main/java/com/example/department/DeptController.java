package com.example.department;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dao.DeptDAO;
import com.example.model.Dept;

@RestController
public class DeptController {
 
  @Autowired
  DeptDAO deptDAO;
  @RequestMapping("Dept")
  public List<Dept> getAllDepartments() {
		return deptDAO.getAllDepartments();
	} 
  @RequestMapping("/getDeptById/{ID}")
  public Dept getDeptById(@PathVariable("ID") int id)
  {
	  return deptDAO.getDeptBYId(id);
  }
  @RequestMapping("/getDeptByName/{Name}")
  public Dept getDeptByName(@PathVariable("Name") String name)
  {
	  return deptDAO.getDeptByName(name);
  }
  @PostMapping("regDept")
  public Dept registerDept(@RequestBody Dept dept)
  {
	  return deptDAO.registerDept(dept);
  }
  @PutMapping("updateDept")
  public Dept updateDepartment(@RequestBody Dept dept)
  {
	  return deptDAO.updateDepartment(dept);
  }
  @DeleteMapping("/deleteDept/{ID}")
  public String deleteDeptById(@PathVariable("ID") int id) {
      deptDAO.deleteDeptById(id);
      return "Product with ID " + id + " deleted successfully";
  }
}
