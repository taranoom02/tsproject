package com.example.department;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.dao.CustomerDAO;
import com.example.model.Customer;



@RestController

@CrossOrigin(origins = "http://localhost:4200")  // Add this line
public class CustomerController {
    @Autowired
    CustomerDAO customerDAO;
    @PostMapping("/regCustomer")
    public Customer registerCustomer(@RequestBody Customer customer)
    {
  	  return customerDAO.registerCustomer(customer);
    }
    
    

    @GetMapping("/getCustByEmailAndPassword/{email}/{password}")
    public Customer getCustByEmailAndPassword(
            @PathVariable("email") String email,
            @PathVariable("password") String password) {
        return customerDAO.getCustByEmailAndPassword(email, password);
    }
    @RequestMapping("Cust")
    public List<Customer> getAllCustomers() {
  		return customerDAO.getAllCustomers();
  	} 
    @PostMapping("changepassword")
    public String changePassword(@RequestBody Map<String, String> request) {
        String email = request.get("email");
        String newPassword = request.get("newPassword");

        Customer customer = customerDAO.changePassword(email, newPassword);

        if (customer != null) {
            return "{\"status\":\"success\",\"message\":\"Password changed successfully\"}";
        } else {
            return "{\"status\":\"error\",\"message\":\"Failed to change password\"}";
        }
    }
    @DeleteMapping("/deleteCustomerById/{id}")
    public void deleteCustomerById(@PathVariable int id) {
    	System.out.println(id);
        customerDAO.deleteCustomerById(id);
    }
    
        

}



