package com.example.department;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dao.EmpDAO;

import com.example.model.Employee;

@RestController
public class EmpController {
@Autowired
EmpDAO empdao;
@RequestMapping("Emp")
public List<Employee> getAllEmployees()
{
	return empdao.getAllEmployees();
}
@RequestMapping("/getEmpById/{Id}")
public Employee getEmployeeById(@PathVariable("Id") int id)
{
	return empdao.getEmployeeById(id);
}
@RequestMapping("/getEmpByName/{Name}")
public Employee getEmployeeByName(@PathVariable("Name") String name)
{
	return empdao.getEmployeeByName(name);
}
@PostMapping("regEmp")
public Employee registerEmployee(@RequestBody Employee emp)
{
	return empdao.registerEmployee(emp);
}
@PutMapping("updateEmp")
public Employee updateEmployee(@RequestBody Employee emp)
{
	return empdao.updateEmployee(emp);
}
@DeleteMapping("/deleteEmp/{Id}")

public void deleteEmployee(@PathVariable("Id") int id)
{
	empdao.deleteEmployee(id);
}
}
