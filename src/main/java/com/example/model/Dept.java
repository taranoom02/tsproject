package com.example.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Dept {
	@Id
private int deptId;
private String deptName;
private String Location;
public Dept() {
	super();
}
public Dept(int deptId, String deptName, String location) {
	super();
	this.deptId = deptId;
	this.deptName = deptName;
	Location = location;
}
public int getDeptId() {
	return deptId;
}
public void setDeptId(int deptId) {
	this.deptId = deptId;
}
public String getDeptName() {
	return deptName;
}
public void setDeptName(String deptName) {
	this.deptName = deptName;
}
public String getLocation() {
	return Location;
}
public void setLocation(String location) {
	Location = location;
}
@Override
public String toString() {
	return "Dept [deptId=" + deptId + ", deptName=" + deptName + ", Location=" + Location + "]";
}

}

