package com.example.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Employee {
	@Id
private int empId;
private String empName;
private double salary;
@ManyToOne
@JoinColumn(name = "deptId", referencedColumnName = "deptId")
private Dept department;
public Employee() {
	super();
}
public Employee(int empId, String empName, double salary, Dept department) {
	super();
	this.empId = empId;
	this.empName = empName;
	this.salary = salary;
	this.department = department;
}
public int getEmpId() {
	return empId;
}
public void setEmpId(int empId) {
	this.empId = empId;
}
public String getEmpName() {
	return empName;
}
public void setEmpName(String empName) {
	this.empName = empName;
}
public double getSalary() {
	return salary;
}
public void setSalary(double salary) {
	this.salary = salary;
}
public Dept getDepartment() {
	return department;
}
public void setDepartment(Dept department) {
	this.department = department;
}
@Override
public String toString() {
	return "Employee [empId=" + empId + ", empName=" + empName + ", salary=" + salary + ", department=" + department
			+ "]";
}


}
