

package com.example.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String custEmail;
    private String password;
    private String custName;
    private String custPhone;
    private String confirmPassword;
	public Customer() {
		super();
	}
	public Customer(Long id, String custEmail, String password, String custName, String custPhone,
			String confirmPassword) {
		super();
		this.id = id;
		this.custEmail = custEmail;
		this.password = password;
		this.custName = custName;
		this.custPhone = custPhone;
		this.confirmPassword = confirmPassword;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustEmail() {
		return custEmail;
	}
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustPhone() {
		return custPhone;
	}
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	@Override
	public String toString() {
		return "Customer [id=" + id + ", custEmail=" + custEmail + ", password=" + password + ", custName=" + custName
				+ ", custPhone=" + custPhone + ", confirmPassword=" + confirmPassword + "]";
	}
	
    
   }
