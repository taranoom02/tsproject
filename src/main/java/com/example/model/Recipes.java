package com.example.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Recipes {
	@Id
	@GeneratedValue
	private int recipeId;
    private String recipeImage;
    private String recipeName;
      
     private String ytLink;

	public Recipes(int recipeId, String recipeImage, String recipeName, String ytLink) {
		super();
		this.recipeId = recipeId;
		this.recipeImage = recipeImage;
		this.recipeName = recipeName;
		this.ytLink = ytLink;
	}

	public Recipes() {
		super();
	}

	public int getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}

	public String getRecipeImage() {
		return recipeImage;
	}

	public void setRecipeImage(String recipeImage) {
		this.recipeImage = recipeImage;
	}

	public String getRecipeName() {
		return recipeName;
	}

	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}

	public String getYtLink() {
		return ytLink;
	}

	public void setYtLink(String ytLink) {
		this.ytLink = ytLink;
	}

	@Override
	public String toString() {
		return "Recipes [recipeId=" + recipeId + ", recipeImage=" + recipeImage + ", recipeName=" + recipeName
				+ ", ytLink=" + ytLink + "]";
	}
  
	
}
