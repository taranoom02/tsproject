package com.example.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.model.Recipes;

@Repository
public interface RecipesRepository extends JpaRepository<Recipes,Integer> {
	List<Recipes> findByRecipeName(String recipeName);
}
