package com.example.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.model.Customer;
import com.example.model.Dept;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	@Query("from Customer c where c.custName=:cName")
	Optional<Customer> findByName(@Param("cName") String name);
	  Customer save(Customer register);
    Customer findByCustEmailAndPassword(String email, String password);
    Optional<Customer> findBycustEmail(String email);
	
} 