package com.example.dao;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Dept;


@Service
public class DeptDAO {
@Autowired
DeptRepository deptRepo;

public List<Dept> getAllDepartments() {
	// TODO Auto-generated method stub
	return deptRepo.findAll();
}

public Dept getDeptBYId(int id) {
	// TODO Auto-generated method stub
	return deptRepo.findById(id).orElse(new Dept());
}

public Dept getDeptByName(String name) {
	// TODO Auto-generated method stub
	return deptRepo.findByName(name);
}

public Dept registerDept(Dept dept) {
	// TODO Auto-generated method stub
	return deptRepo.save(dept);
}

public Dept updateDepartment(Dept dept) {
	// TODO Auto-generated method stub
	return deptRepo.save(dept);
}

public void deleteDeptById(int id) {
	// TODO Auto-generated method stub
	deptRepo.deleteById(id);
}

}
