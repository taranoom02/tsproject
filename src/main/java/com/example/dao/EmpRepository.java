package com.example.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.model.Dept;
import com.example.model.Employee;

@Repository
public interface EmpRepository extends JpaRepository<Employee,Integer>
{
	@Query("from Employee e where e.empName=:eName")
	Employee findByName(@Param("eName") String name);
}
