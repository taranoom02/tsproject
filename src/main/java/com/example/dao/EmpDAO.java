package com.example.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Dept;
import com.example.model.Employee;

@Service
public class EmpDAO {
@Autowired
EmpRepository empRepo;

public List<Employee> getAllEmployees() {
	// TODO Auto-generated method stub
	return empRepo.findAll();
}

public Employee getEmployeeById(int id) {
	// TODO Auto-generated method stub
	return empRepo.findById(id).orElse(new Employee());
}

public Employee getEmployeeByName(String name) {
	// TODO Auto-generated method stub
	return empRepo.findByName(name);
}

public Employee registerEmployee(Employee emp) {
	// TODO Auto-generated method stub
	return empRepo.save(emp);
}

public Employee updateEmployee(Employee emp) {
	// TODO Auto-generated method stub
	return empRepo.save(emp);
}

public void deleteEmployee(int id) {
	// TODO Auto-generated method stub
	empRepo.deleteById(id);
}
}
