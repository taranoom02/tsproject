// CustomerDAO.java
package com.example.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import com.example.model.Customer;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerDAO {

    private final CustomerRepository customerRepo;
    private final JavaMailSender mailSender;

    @Autowired
    public CustomerDAO(CustomerRepository customerRepo, JavaMailSender mailSender) {
        this.customerRepo = customerRepo;
        this.mailSender = mailSender;
    }

    public Customer registerCustomer(Customer customer) {
        Customer savedCustomer = customerRepo.save(customer);

        // Send a confirmation email
        sendWelcomeEmail(savedCustomer);
        System.out.println("Confirmation email sent.");

        return savedCustomer;
    }

    public Customer getCustByEmailAndPassword(String email, String password) {
        return customerRepo.findByCustEmailAndPassword(email, password);
    }

    public List<Customer> getAllCustomers() {
        return customerRepo.findAll();
    }

    public Customer changePassword(String email, String newPassword) {
        Customer customer = customerRepo.findBycustEmail(email).orElse(null);

        if (customer != null) {
            customer.setPassword(newPassword);
            return customerRepo.save(customer);
        } else {
            return null; // Handle case where the customer is not found
        }
    }

    private void sendWelcomeEmail(Customer customer) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(customer.getCustEmail());
        message.setSubject("Welcome to our website");
        message.setText("Dear " + customer.getCustName() + ",\n\n"
                + "Thank you for registering ");

        mailSender.send(message);
    }

    public void deleteCustomerById(long id) {
        customerRepo.deleteById(id);
    }

    	
}
