package com.example.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Recipes;
@Service
public class RecipesDAO {
@Autowired
RecipesRepository recipesRepo;
	public List<Recipes> getAllRecipes() {
		// TODO Auto-generated method stub
		return recipesRepo.findAll();
	}

	public List<Recipes> getRecipesByName(String recipeName) {
		// TODO Auto-generated method stub
		return recipesRepo.findByRecipeName(recipeName);
	}

}
